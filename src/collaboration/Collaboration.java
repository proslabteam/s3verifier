package collaboration;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.stream.events.EndElement;


import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.impl.instance.EventBasedGatewayImpl;
import org.camunda.bpm.model.bpmn.impl.instance.ParticipantRef;
import org.camunda.bpm.model.bpmn.impl.instance.SubProcessImpl;
import org.camunda.bpm.model.bpmn.instance.BaseElement;
import org.camunda.bpm.model.bpmn.instance.BpmnModelElementInstance;
import org.camunda.bpm.model.bpmn.instance.BusinessRuleTask;
import org.camunda.bpm.model.bpmn.instance.EndEvent;
import org.camunda.bpm.model.bpmn.instance.EventBasedGateway;
import org.camunda.bpm.model.bpmn.instance.ExclusiveGateway;
import org.camunda.bpm.model.bpmn.instance.FlowNode;
import org.camunda.bpm.model.bpmn.instance.InclusiveGateway;
import org.camunda.bpm.model.bpmn.instance.IntermediateCatchEvent;
import org.camunda.bpm.model.bpmn.instance.IntermediateThrowEvent;
import org.camunda.bpm.model.bpmn.instance.ManualTask;
import org.camunda.bpm.model.bpmn.instance.Message;
import org.camunda.bpm.model.bpmn.instance.MessageFlow;
import org.camunda.bpm.model.bpmn.instance.ParallelGateway;
import org.camunda.bpm.model.bpmn.instance.ReceiveTask;
import org.camunda.bpm.model.bpmn.instance.ScriptTask;
import org.camunda.bpm.model.bpmn.instance.SendTask;
import org.camunda.bpm.model.bpmn.instance.SequenceFlow;
import org.camunda.bpm.model.bpmn.instance.ServiceTask;
import org.camunda.bpm.model.bpmn.instance.StartEvent;
import org.camunda.bpm.model.bpmn.instance.SubProcess;
import org.camunda.bpm.model.bpmn.instance.UserTask;
import org.camunda.bpm.model.xml.instance.ModelElementInstance;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;

import utils.LTSNode;
import utils.LTSNode.NodeType;
import utils.Utils;





public class Collaboration {

	private BpmnModelInstance modelInstance;
	private SingleGraph collaborationGraph;
	private Collection<String> choreographyActions;
	private static boolean cycle;
	private boolean containMessage=false, isSource=false;
	public static  Integer  collaborationCounter;
	public static ArrayList<LTSNode> nodeSet;
	public static String collaborationPath;
	long TimeOut=Long.MAX_VALUE;
	boolean stop=false;
	long start_time;


	public static void main(String[] args) {
		Collaboration collaboration = new Collaboration();
		JFileChooser chooser = new JFileChooser(".");
		FileNameExtensionFilter filter = new FileNameExtensionFilter("BPMN", "bpmn");
		chooser.setFileFilter(filter);
		int returnVal = chooser.showOpenDialog(null);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			System.out.println("You chose to open this file: " + chooser.getSelectedFile().getName());
		}
		File file = new File(chooser.getSelectedFile().getAbsolutePath());
		//		File file=new File("/home/cippus/Documents/Collaboration.bpmn");
		System.out.println(file.getAbsolutePath());

		try {
			collaboration.init(new FileInputStream(file), true);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}

	public String timedInit(InputStream input, boolean show, long timeOut){
		TimeOut=timeOut;
		return init(input, show);
	}

	public String init(InputStream input, boolean show) {
		collaborationGraph=Utils.GenerateGraph("Collaboration");
		if (show) {
			Utils.showGraph(collaborationGraph);
		}
		modelInstance = Bpmn.readModelFromStream(input);
		LTSNode root=new LTSNode(NodeType.COL);
		root.setLabel("Root");
		collaborationGraph.addNode(String.valueOf(root.getId())).setAttribute("ui.label", root.getId());





		//Create transitions from the root node to the starEvent in the BPMN model
		for (StartEvent startEvent : modelInstance.getModelElementsByType(StartEvent.class)) {

			if (!(startEvent.getParentElement() instanceof SubProcess)) {
				FlowNode root1 = Utils.createElement(startEvent.getScope(), "0", StartEvent.class, modelInstance);
				SequenceFlow s = Utils.createSequenceFlow(modelInstance, root1, startEvent);
				root.addEdge(s);
			}

		}
		start_time=System.currentTimeMillis();
		DFS(root);


		if (!stop) {
			collaborationPath=Utils.generatemAUTFile(collaborationGraph);
			long start =System.currentTimeMillis();
			String result="";
			result= checkProperties();
			long stop=System.currentTimeMillis();
			result+=" && "+ ((stop-start)/1000.0);
			System.out.println(result);
			return result;
		}else{
			return "timeout";
		}


	}

	public String checkProperties(){
		//SOUNDNESS CHECKS

		//leaf LTS
		Hashtable<Node, LTSNode> ltsLeaves = new Hashtable<Node, LTSNode>();
		Set<String> endSequenceFlowLTS=new HashSet<String>();
		for(Node n : collaborationGraph.getNodeSet()) {
			if(n.getOutDegree()==0) {
				ltsLeaves.put(n, nodeSet.get(Integer.valueOf(n.getId())-1));
				for (Edge edge : n.getEachEnteringEdge()) {
					endSequenceFlowLTS.add((String) edge.getAttribute("ui.label"));
				}
				System.out.print(n+"         ");
				System.out.println(nodeSet.get(Integer.valueOf(n.getId())-1));
			}
		}

		//INCOMIN SEQUENCE FLOW  END EVENT 
		Set<String> endSequenceFlowBPMN=new HashSet<String>();
		Hashtable<String, Integer> find = new Hashtable<String, Integer>();
		for(EndEvent e:modelInstance.getModelElementsByType(EndEvent.class)) {
			if (!(e.getParentElement() instanceof SubProcess)) {
				for (SequenceFlow s:e.getIncoming()) {
					find.put(s.getId(), 0);
					endSequenceFlowBPMN.add(s.getId());
				}
			}


		}	
		String result="";
		//SOUNDNESS GLOBALE
		Set<String> properCompletion=Utils.propCompletion(collaborationGraph.getNode(0), find);

		if(ltsLeaves.keySet().size() == 1 && properCompletion.size()==0&& endSequenceFlowBPMN.equals(endSequenceFlowLTS) &&((LTSNode)ltsLeaves.values().toArray()[0]).getEdges().keySet().size()==0 && ((LTSNode)ltsLeaves.values().toArray()[0]).getMessages().keySet().size()==0) {


			//SOUND
			result+="3\n";	
		}
		else if( properCompletion.size()==0&& endSequenceFlowBPMN.equals(endSequenceFlowLTS) && checkSequenceFlow(ltsLeaves.values())==0) {
			//RELAXED SOUND

			result+="2\n";	
			for (Node node : ltsLeaves.keySet()) {
				for (MessageFlow m : ltsLeaves.get(node).getMessages().keySet()) {
					result+="++"+m.getId()+"\n";
				}
			}
		}

		else if (ltsLeaves.keySet().size()==1 && properCompletion.size()!=0&& endSequenceFlowBPMN.equals(endSequenceFlowLTS) && ((LTSNode)ltsLeaves.values().toArray()[0]).getMessages().keySet().size()==0&& ((LTSNode)ltsLeaves.values().toArray()[0]).getMessages().keySet().size()>0){
			//	UNSOUND PROPER COMPLETION VIOLATED
			System.out.println(properCompletion);
			result+="1\n";
			for (Node node : ltsLeaves.keySet()) {
				for (MessageFlow m : ltsLeaves.get(node).getMessages().keySet()) {
					result+="++"+m.getId()+"\n";
				}
				for (SequenceFlow s : ltsLeaves.get(node).getEdges().keySet()) {
					result+="--"+s.getId()+"\n";
				}
				for (String s : properCompletion) {
					result+="--"+s+"\n";
				}

			}
		}
		else {
			//			MODEL UNSOUND FOR POSSIBLE DEAD TOKEN IN:
			result+="0\n";
			for (Node node : ltsLeaves.keySet()) {
				for (MessageFlow m : ltsLeaves.get(node).getMessages().keySet()) {
					result+="++"+m.getId()+"\n";
				}
				for (SequenceFlow s : ltsLeaves.get(node).getEdges().keySet()) {
					result+="--"+s.getId()+"\n";
				}
			}
			for (String s : properCompletion) {
				result+="--"+s+"\n";
			}

		}
		result+=" && \n";
		boolean safe=true;
		Set<SequenceFlow>violatingSeqFlow=new HashSet();
		for (LTSNode n : nodeSet) {
			for (SequenceFlow s : n.getEdges().keySet()) {
				if (n.getEdges().get(s)>1) {
					violatingSeqFlow.add(s);
					safe=false;
				}
			}
		}
		if (safe) {
			//SAFE
			result+="4\n";
		}else{
			//UNSAFE
			result+="5\n";
			for (SequenceFlow sequenceFlow : violatingSeqFlow) {
				result+="++"+sequenceFlow.getId()+"\n";
			}
		}


		return result;

		//Utils.allTrace(collaborationGraph.getNode(0),"0");
	}



	private int checkSequenceFlow(Collection<LTSNode> ltsLeaves){
		int counter=0;
		for (LTSNode n : ltsLeaves) {
			for (SequenceFlow s : n.getEdges().keySet()) {
				if (s.getId().startsWith("0-")) {
					System.out.println(s.getId()+"aaaaaaa");
				}else{
					counter++;
				}

			}

		}
		return counter;
	}



	public Collaboration() {
		collaborationCounter=0;
		nodeSet=new ArrayList<LTSNode>();
	}

	private void DFS(LTSNode currentNode) {



		Enumeration<SequenceFlow> edges = currentNode.getEdges().keys();
		while (edges.hasMoreElements()&& !stop) {
			if ((System.currentTimeMillis()-start_time)>TimeOut) {
				stop=true;
			}



			//			try{

			SequenceFlow edge= edges.nextElement();
			FlowNode currentElement=edge.getTarget();


			String edgeLabel=edge.getId();


			//STARTEVENT
			if (currentElement instanceof StartEvent && getMessageFlow(currentElement)==null) {
				for (SequenceFlow outgoingEdge : currentElement.getOutgoing()) {
					LTSNode nextNode=new LTSNode(NodeType.COL);
					nextNode.clone(currentNode);
					nextNode.decreaseEdge(edge);
					nextNode.addEdge(outgoingEdge);
					nextNode=nextNode.validate();
					collaborationGraph.addEdge(currentNode.getId()+"_"+ nextNode.getId(), String.valueOf(currentNode.getId()), String.valueOf(nextNode.getId()), true).setAttribute("ui.label", edgeLabel);;
					collaborationGraph.getNode(nextNode.getId()).setAttribute("ui.label", nextNode.getId());
					DFS(nextNode);
				}
			}
			//SERVICETASK
			if (currentElement instanceof ServiceTask ||
					currentElement instanceof UserTask ||
					currentElement instanceof ScriptTask ||
					currentElement instanceof BusinessRuleTask ||
					currentElement instanceof ManualTask ||
					currentElement.getClass().getSimpleName().equals("TaskImpl")) {
				if(getMessageFlow(currentElement)==null){
					for (SequenceFlow outgoingEdge : currentElement.getOutgoing()) {
						LTSNode nextNode=new LTSNode(NodeType.COL);
						nextNode.clone(currentNode);
						nextNode.decreaseEdge(edge);
						nextNode.addEdge(outgoingEdge);
						nextNode=nextNode.validate();
						collaborationGraph.addEdge(currentNode.getId()+"_"+ nextNode.getId(), String.valueOf(currentNode.getId()), String.valueOf(nextNode.getId()), true).setAttribute("ui.label", edgeLabel);
						collaborationGraph.getNode(nextNode.getId()).setAttribute("ui.label", nextNode.getId());
						DFS(nextNode);
					}
				}
			}
			//ENDEVENT
			if (currentElement instanceof EndEvent) {
				if (currentElement.getParentElement() instanceof SubProcess) {
					SubProcess s=(SubProcess) currentElement.getParentElement();
					Set<SequenceFlow> subSeqFlow=new HashSet<SequenceFlow>();
					for (FlowNode f: s.getChildElementsByType(FlowNode.class)) {
						subSeqFlow.addAll(f.getIncoming());
						subSeqFlow.addAll(f.getOutgoing());
					}

					LTSNode nextNode=new LTSNode(NodeType.COL);
					nextNode.clone(currentNode);
					nextNode.decreaseEdge(edge);


					boolean subProcesEnded=true;
					for (SequenceFlow sequenceFlow : nextNode.getEdges().keySet()) {
						if (subSeqFlow.contains(sequenceFlow)) {
							subProcesEnded=false;
							break;
						}
					}
					if (subProcesEnded) {
						nextNode.addEdge((SequenceFlow) s.getOutgoing().toArray()[0]);
					}
					nextNode=nextNode.validate();
					collaborationGraph.addEdge(currentNode.getId()+"_"+ nextNode.getId(), String.valueOf(currentNode.getId()), String.valueOf(nextNode.getId()), true).setAttribute("ui.label", edgeLabel);
					collaborationGraph.getNode(nextNode.getId()).setAttribute("ui.label", nextNode.getId());
					DFS(nextNode);


				}else{
					LTSNode nextNode=new LTSNode(NodeType.COL);
					nextNode.clone(currentNode);
					nextNode.decreaseEdge(edge);
					nextNode=nextNode.validate();
					collaborationGraph.addEdge(currentNode.getId()+"_"+ nextNode.getId(), String.valueOf(currentNode.getId()), String.valueOf(nextNode.getId()), true).setAttribute("ui.label", edgeLabel);
					collaborationGraph.getNode(nextNode.getId()).setAttribute("ui.label", nextNode.getId());

					DFS(nextNode);
				}

			}

			//SUBPROCESS
			if (currentElement instanceof SubProcess) {
				for (StartEvent start: currentElement.getChildElementsByType(StartEvent.class)) {
					for (SequenceFlow outgoingEdge : start.getOutgoing()) {
						LTSNode nextNode=new LTSNode(NodeType.COL);
						nextNode.clone(currentNode);
						nextNode.decreaseEdge(edge);
						nextNode.addEdge(outgoingEdge);
						nextNode=nextNode.validate();
						collaborationGraph.addEdge(currentNode.getId()+"_"+ nextNode.getId(), String.valueOf(currentNode.getId()), String.valueOf(nextNode.getId()), true).setAttribute("ui.label", edgeLabel);
						collaborationGraph.getNode(nextNode.getId()).setAttribute("ui.label", nextNode.getId());
						DFS(nextNode);
					}


				}

			}
			//SENDTASK

			if (currentElement instanceof SendTask || currentElement instanceof IntermediateThrowEvent||
					(currentElement instanceof ServiceTask ||
							currentElement instanceof UserTask ||
							currentElement instanceof ScriptTask ||
							currentElement instanceof BusinessRuleTask ||
							currentElement instanceof ManualTask ||
							currentElement.getClass().getSimpleName().equals("TaskImpl")) 
					&& getMessageFlow(currentElement)!=null && isSource ) {
				for (SequenceFlow outgoingEdge : currentElement.getOutgoing()) {
					LTSNode nextNode=new LTSNode(NodeType.COL);
					nextNode.clone(currentNode);
					nextNode.decreaseEdge(edge);
					nextNode.addEdge(outgoingEdge);
					nextNode.addMessages(getMessageFlow(currentElement));
					nextNode=nextNode.validate();
					collaborationGraph.addEdge(currentNode.getId()+"_"+ nextNode.getId(), String.valueOf(currentNode.getId()), String.valueOf(nextNode.getId()), true).setAttribute("ui.label", edgeLabel);
					collaborationGraph.getNode(nextNode.getId()).setAttribute("ui.label", nextNode.getId());
					DFS(nextNode);
				}
			}

			//RECEIVETASK
			if (currentElement instanceof ReceiveTask || currentElement instanceof IntermediateCatchEvent ||(currentElement instanceof ServiceTask ||
					currentElement instanceof StartEvent ||//nel caso  di start event message
					currentElement instanceof UserTask ||
					currentElement instanceof ScriptTask ||
					currentElement instanceof BusinessRuleTask ||
					currentElement instanceof ManualTask ||
					currentElement.getClass().getSimpleName().equals("TaskImpl")) 
					&& getMessageFlow(currentElement)!=null && !isSource ) {
				for (SequenceFlow outgoingEdge : currentElement.getOutgoing()) {
					MessageFlow message=getMessageFlow(currentElement);
					if(currentNode.decreaseMessage(message)){
						LTSNode nextNode=new LTSNode(NodeType.COL);
						nextNode.clone(currentNode);
						//trucco per rimettere il messaggio nel current node dopo che è stato tolto prima di fare il clone
						currentNode.addMessages(message);
						nextNode.decreaseEdge(edge);
						nextNode.addEdge(outgoingEdge);
						//SE IL RECEIVETASK È FILGIO DI UN EVENT BASE DEVE DISATTIVARE L'ALTRO EDGE CHE NON È UTILIZZATO
						if (currentElement.getPreviousNodes().list().get(0) instanceof EventBasedGateway ||currentElement.getPreviousNodes().list().get(0) instanceof IntermediateCatchEvent) {
							BaseElement eventbase=(BaseElement) currentElement.getPreviousNodes().list().get(0);
							//POTREBBE SERVIRE NEL CASO DI CLICLI
							//        					for (MessageFlow mes : nextNode.getMessages().keySet()) {
							//        						if (mes.getSource().getId().equals(eventbase.getId())) {
							//									nextNode.decreaseMessage(mes);
							//								}
							//							}
							Enumeration<SequenceFlow> seqEnum = nextNode.getEdges().keys();
							while (seqEnum.hasMoreElements()) {
								SequenceFlow seq=seqEnum.nextElement();
								if (seq.getSource().getId().equals(eventbase.getId())) {
									nextNode.decreaseEdge(seq);
								}
							}
						}
						
						nextNode=nextNode.validate();
					
						collaborationGraph.addEdge(currentNode.getId()+"_"+ nextNode.getId(), String.valueOf(currentNode.getId()), String.valueOf(nextNode.getId()), true).setAttribute("ui.label", message.getSource().getParentElement().getAttributeValue("name")+"->"+message.getTarget().getParentElement().getAttributeValue("name")+":"+message.getName());
					
						collaborationGraph.getNode(nextNode.getId()).setAttribute("ui.label", nextNode.getId());
						DFS(nextNode);
					}
				}
			}

			//PARALLELGATEWAY
			if(currentElement instanceof ParallelGateway){
				//        		DIVERGING
				if (currentElement.getIncoming().size()==1) {
					LTSNode nextNode=new LTSNode(NodeType.COL);
					nextNode.clone(currentNode);
					nextNode.decreaseEdge(edge);
					for (SequenceFlow outgoingEdge : currentElement.getOutgoing()) {
						nextNode.addEdge(outgoingEdge);

					}
					nextNode=nextNode.validate();
					collaborationGraph.addEdge(currentNode.getId()+"_"+ nextNode.getId(), String.valueOf(currentNode.getId()), String.valueOf(nextNode.getId()), true).setAttribute("ui.label", edgeLabel);
					collaborationGraph.getNode(nextNode.getId()).setAttribute("ui.label", nextNode.getId());
					DFS(nextNode);
				}
				//        		CONVERGING
				if (currentElement.getOutgoing().size()==1) {
					boolean enabled=false;
					for (SequenceFlow incomingEdge : currentElement.getIncoming()) {
						if (currentNode.getEdges().containsKey(incomingEdge)) {
							enabled=true;
						}else{
							enabled=false;
							break;
						}
					}
					if (enabled) {
						LTSNode nextNode=new LTSNode(NodeType.COL);
						nextNode.clone(currentNode);
						
						for (SequenceFlow incomingEdge : currentElement.getIncoming()) {
							nextNode.decreaseEdge(incomingEdge);
						}
						for (SequenceFlow outgoingEdge : currentElement.getOutgoing()) {
							nextNode.addEdge(outgoingEdge);
						}
						nextNode=nextNode.validate();
						collaborationGraph.addEdge(currentNode.getId()+"_"+ nextNode.getId(), String.valueOf(currentNode.getId()), String.valueOf(nextNode.getId()), true).setAttribute("ui.label", edgeLabel);
						collaborationGraph.getNode(nextNode.getId()).setAttribute("ui.label", nextNode.getId());
						DFS(nextNode);
					}

				}
			}
			//XORGATEWAY
			if(currentElement instanceof ExclusiveGateway){
				//        		DIVERGING
				for (SequenceFlow outgoingEdge : currentElement.getOutgoing()) {
					LTSNode nextNode=new LTSNode(NodeType.COL);
					nextNode.clone(currentNode);
					nextNode.decreaseEdge(edge);
					nextNode.addEdge(outgoingEdge);
					nextNode=nextNode.validate();
					collaborationGraph.addEdge(currentNode.getId()+"_"+ nextNode.getId(), String.valueOf(currentNode.getId()), String.valueOf(nextNode.getId()), true).setAttribute("ui.label", edgeLabel);
					collaborationGraph.getNode(nextNode.getId()).setAttribute("ui.label", nextNode.getId());
					DFS(nextNode);

				}
			}



			//EVENTBASEDGATEWAY
			if(currentElement instanceof EventBasedGateway){
				//        	    		DIVERGING
				if (currentElement.getIncoming().size()==1) {
					LTSNode nextNode=new LTSNode(NodeType.COL);
					nextNode.clone(currentNode);
					nextNode.decreaseEdge(edge);
					for (SequenceFlow outgoingEdge : currentElement.getOutgoing()) {
						nextNode.addEdge(outgoingEdge);

					}
					nextNode=nextNode.validate();
					collaborationGraph.addEdge(currentNode.getId()+"_"+ nextNode.getId(), String.valueOf(currentNode.getId()), String.valueOf(nextNode.getId()), true).setAttribute("ui.label", edgeLabel);
					collaborationGraph.getNode(nextNode.getId()).setAttribute("ui.label", nextNode.getId());
					DFS(nextNode);
				}
			}

		}



	}

	public MessageFlow getMessageFlow(FlowNode currentElement){
		for (MessageFlow message : modelInstance.getModelElementsByType(MessageFlow.class)) {
			if (message.getSource().getId().equals(currentElement.getId())) {
				containMessage=true;
				isSource=true;
				return message;
			}else if(message.getTarget().getId().equals(currentElement.getId())){
				containMessage=true;
				isSource=false;
				return message;
			}
		}
		containMessage=false;
		return null;
	}

	public void setChoreographyActions(ArrayList<String> choreographyActions2) {
		this.choreographyActions=choreographyActions2;
	}





}
