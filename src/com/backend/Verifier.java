package com.backend;  

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces; 
import javax.ws.rs.core.MediaType;

import org.apache.commons.io.IOUtils;

import collaboration.Collaboration;

@Path("/BPMN") 

public class Verifier {  
  
   
   @POST
   @Path("/Verifier") 
   @Produces(MediaType.TEXT_PLAIN)
   public String getUsers(String model){ 
	 //  System.out.println(model);
	   
	   Collaboration collaboration = new Collaboration();
		
		try {
			File f=File.createTempFile(model, null);
			return collaboration.init(IOUtils.toInputStream(model, "UTF-8"), false);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		return "Something was catched";
   }  
}